import React, { Fragment } from 'react';

import styled from 'styled-components';


import { Container } from 'react-bootstrap'
// eslint-disable-next-line no-unused-vars
import Job from './components/job'


export const Home = () => (
 
   <Container>
 <styles>
 <div><h2>Way Fellow</h2></div>
  <Fragment> <input type="text" className="search-bar" placeholder="Search..." /></Fragment>           

 </styles>
 </Container>
 
)




const styles =styled.div`
  .search-bar .form-control {
    line-height: 2.25rem;
    height: 50px;
    font-size: 0.9rem;
    color: black !important;
    max-width: 600px;
    margin: 0 auto;
    background-color: #f5f8fa;
    /* box-shadow: 2px 2px 3px 0px rgba(102, 102, 102, 0.5); */
    border-radius: 30px;
    padding-left: 45px;
  }
  .form-control {
    background-color: #E6ECF0;
    line-height: 25px;
  }
  .form-control {
    display: block;
    width: 100%;
    padding: 0.375rem 0.75rem;
    font-size: 0.85rem;
    color: #495057;
    background-color: #F5F8FA;
    background-clip: padding-box;
    /* border: 1px solid #3C6077; */
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    -webkit-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    -moz-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    -ms-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    -o-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  }
  .form-control {
    display: block;
    width: 100%;
    height: calc(1.5em + .75rem + 2px);
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
  }
  button, input {
    overflow: visible;
  }
  input, button, select, optgroup, textarea {
    margin: 0;
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
  }
  button, input {
    overflow: visible;
  }
  button, input, optgroup, select, textarea {
    margin: 0;
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
  }
  button, input {
    overflow: visible;
  }
  button, input, optgroup, select, textarea {
    margin: 0;
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
  }
  *, *::before, *::after {
    box-sizing: border-box;
  }
  *, ::after, ::before {
    box-sizing: inherit;
  }
  *, ::after, ::before {
    box-sizing: border-box;
  }
  *, ::after, ::before {
    box-sizing: border-box;
  }
  user agent stylesheet
  input {
    -webkit-writing-mode: horizontal-tb !important;
    text-rendering: auto;
    color: -internal-light-dark-color(black, white);
    letter-spacing: normal;
    word-spacing: normal;
    text-transform: none;
    text-indent: 0px;
    text-shadow: none;
    display: inline-block;
    text-align: start;
    -webkit-appearance: textfield;
    background-color: -internal-light-dark-color(white, black);
    -webkit-rtl-ordering: logical;
    cursor: text;
    margin: 0em;
    font: 400 13.3333px Arial;
    padding: 1px 0px;
    border-width: 2px;
    border-style: inset;
    border-color: initial;
    border-image: initial;
  } `;


