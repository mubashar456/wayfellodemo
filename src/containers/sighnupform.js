import React, { Component, Fragment } from "react";
import Input from "../components/Input";
import { Link } from 'react-router-dom';
import CheckBox from "../components/checkbox";
import Button from "../components/Button";

export  class SignupContainer extends Component {
        // eslint-disable-next-line no-useless-constructor
        constructor(props) {
          super(props);
        
        this.state = {
                newUser: {
                  name: "",
                  age: "",
                  gender: "",
                  skills: [],
                  about: ""
                },
          
                genderOptions: ["Male", "Female", "Others"],
                skillOptions: ["Programming", "Development", "Design", "Testing"]
              };
              this.handleTextArea = this.handleTextArea.bind(this);
              this.handleAge = this.handleAge.bind(this);
              this.handleFullName = this.handleFullName.bind(this);
              this.handleFormSubmit = this.handleFormSubmit.bind(this);
              this.handleClearForm = this.handleClearForm.bind(this);
              this.handleCheckBox = this.handleCheckBox.bind(this);
              this.handleInput = this.handleInput.bind(this);
            }
          
            /* This lifecycle hook gets executed when the component mounts */
          
            handleFullName(e) {
              let value = e.target.value;
              this.setState(
                prevState => ({
                  newUser: {
                    ...prevState.newUser,
                    name: value
                  }
                }),
                () => console.log(this.state.newUser)
              );
            }
          
            handleAge(e) {
              let value = e.target.value;
              this.setState(
                prevState => ({
                  newUser: {
                    ...prevState.newUser,
                    age: value
                  }
                }),
                () => console.log(this.state.newUser)
              );
            }
          
            handleInput(e) {
              let value = e.target.value;
              let name = e.target.name;
              this.setState(
                prevState => ({
                  newUser: {
                    ...prevState.newUser,
                    [name]: value
                  }
                }),
                () => console.log(this.state.newUser)
              );
            }
          
            handleTextArea(e) {
              console.log("Inside handleTextArea");
              let value = e.target.value;
              this.setState(
                prevState => ({
                  newUser: {
                    ...prevState.newUser,
                    about: value
                  }
                }),
                () => console.log(this.state.newUser)
              );
            }
          
            handleCheckBox(e) {
              const newSelection = e.target.value;
              let newSelectionArray;
          
              if (this.state.newUser.skills.indexOf(newSelection) > -1) {
                newSelectionArray = this.state.newUser.skills.filter(
                  s => s !== newSelection
                );
              } else {
                newSelectionArray = [...this.state.newUser.skills, newSelection];
              }
          
              this.setState(prevState => ({
                newUser: { ...prevState.newUser, skills: newSelectionArray }
              }));
            }
          
            handleFormSubmit(e) {
              e.preventDefault();
              let userData = this.state.newUser;
          
              fetch("http://example.com", {
                method: "POST",
                body: JSON.stringify(userData),
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json"
                }
              }).then(response => {
                response.json().then(data => {
                  console.log("Successful" + data);
                });
              });
            }
          
            handleClearForm(e) {
              e.preventDefault();
              this.setState({
                newUser: {
                  name: "",
                  age: "",
                  gender: "",
                  skills: [],
                  about: ""
                }
              });
            }
        render() {
                return (
                 <form className="container-fluid" onSubmit={this.handleFormSubmit}>       
                <Fragment className="content">
                        <Fragment className="container">
                        <Fragment className="row pt-3 justify-content-center"> 
                        <Fragment className="col-lg-4 col-md-6 col-sm-8">

                                <header className="section-title">
                                <h4 className="pull-left">Sign up</h4>
                                <p className="pull-right">Already have an account? <Link to="https://wayfellow.com/login">Sign in</Link></p>
                                <Fragment className="clearfix"></Fragment>
                                </header>

                          <form key="register">
                                <Input type="hidden" name="_token" value="4KiJTn3EBpnUGzsqv5VYap54qwVJih9XNwVvMmDk" />
                                <Fragment className="first step1 current form-section"></Fragment>
                                <label>Select Account Type</label>
        
                                <Fragment className="btn-group form-group" key="wf-account-type" data-toggle="buttons">
                                        <label className="btn btn-default btn-switcher btn-sm  active ">
                                        <Input type="radio" value="user" name="role" className="hidden" checked="checked" required="" />User</label>
                                        <label className="btn btn-default btn-switcher btn-sm ">
                                        <Input type="radio" value="company" name="role" className="hidden" required="" />Company</label>           
                                </Fragment>

                                <Fragment className="form-group">
                                <label for="wf-name">Full Name</label>
                                <Input type="text" className="form-control " id="wf-name"
                                placeholder="Full name" name="full_name" value="" autocomplete="full_name" required />
                                </Fragment>

                                <Fragment className="form-group">
                                <label for="username">Set up your page name</label>
                                <Input type="text" className="form-control " id="username"
                                placeholder="Page name e.g. wayfellow" name="username" value="" autocomplete="off" required />
                                <span id="usernamepass"></span>
                               </Fragment>

                               <Fragment className="form-group">
                                <label for="password">Password</label>
                                <Input type="password" minlength="8" className="form-control " id="password"
                                placeholder="********" name="password" autocomplete="off" required />
                                <span className="d-block" id="passwordpass"></span>
                                <small className="text-muted">At least one letter, one number, and min 8 characters</small>
                                </Fragment>


                                <Fragment className="form-group">
                                <label for="password-confirmation">Confirm Password</label>
                                <Input type="password" minlength="8" className="form-control"
                                id="password-confirmation" autocomplete="off"
                                placeholder="********" name="confirm_password" required />
                                <small id="message"></small>
                                </Fragment>

                                <Fragment className="form-group mb-1 mt-1">
                                <Input type="checkbox" aria-label="terms and conditions" name="wf-terms" id="wf-terms">
                                 I agree to </Input>
                                <p><Link to="./underdev" className="wf-form-link">wayfellow terms & privacy</Link></p>
                                </Fragment>
                                
                                <Fragment className="form-navigation">
                                <button type="submit" id="submit" className="btn btn-primary w-100">Register</button>
                                </Fragment>
                             
                        </form>
                        </Fragment>                
                        </Fragment>
                        </Fragment>
                </Fragment>
                </form>
        )
        }}
        
        
     
             

