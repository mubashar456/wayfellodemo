import React, { Component, Fragment } from "react";
import {Link } from "react-router-dom";
/* Import Components */
import CheckBox from "../components/checkbox";
import Input from "../components/Input";

import Button from "../components/Button";

export  class RegisterContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newUser: {
        name: "",
        age: "",
        gender: "",
        skills: [],
        about: ""
      },

      genderOptions: ["Male", "Female", "Others"],
      skillOptions: ["i agree to wayfellow terms"]
    };
    this.handleTextArea = this.handleTextArea.bind(this);
    this.handleAge = this.handleAge.bind(this);
    this.handleFullName = this.handleFullName.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleClearForm = this.handleClearForm.bind(this);
    this.handleCheckBox = this.handleCheckBox.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }

  /* This lifecycle hook gets executed when the component mounts */

  handleFullName(e) {
    let value = e.target.value;
    this.setState(
      prevState => ({
        newUser: {
          ...prevState.newUser,
          name: value
        }
      }),
      () => console.log(this.state.newUser)
    );
  }

  handleAge(e) {
    let value = e.target.value;
    this.setState(
      prevState => ({
        newUser: {
          ...prevState.newUser,
          age: value
        }
      }),
      () => console.log(this.state.newUser)
    );
  }

  handleInput(e) {
    let value = e.target.value;
    let name = e.target.name;
    this.setState(
      prevState => ({
        newUser: {
          ...prevState.newUser,
          [name]: value
        }
      }),
      () => console.log(this.state.newUser)
    );
  }

  handleTextArea(e) {
    console.log("Inside handleTextArea");
    let value = e.target.value;
    this.setState(
      prevState => ({
        newUser: {
          ...prevState.newUser,
          about: value
        }
      }),
      () => console.log(this.state.newUser)
    );
  }

  handleCheckBox(e) {
    const newSelection = e.target.value;
    let newSelectionArray;

    if (this.state.newUser.skills.indexOf(newSelection) > -1) {
      newSelectionArray = this.state.newUser.skills.filter(
        s => s !== newSelection
      );
    } else {
      newSelectionArray = [...this.state.newUser.skills, newSelection];
    }

    this.setState(prevState => ({
      newUser: { ...prevState.newUser, skills: newSelectionArray }
    }));
  }

  handleFormSubmit(e) {
    e.preventDefault();
    let userData = this.state.newUser;

    fetch("http://example.com", {
      method: "POST",
      body: JSON.stringify(userData),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    }).then(response => {
      response.json().then(data => {
        console.log("Successful" + data);
      });
    });
  }

  handleClearForm(e) {
    e.preventDefault();
    this.setState({
      newUser: {
        name: "",
        age: "",
        gender: "",
        skills: [],
        about: ""
      }
    });
  }

  render() {
    return (
      <form className="container-fluid" onSubmit={this.handleFormSubmit}>
        <Fragment className="content">
                        <Fragment className="container">
                        <Fragment className="row pt-3 justify-content-center"> 
                        <Fragment className="col-lg-4 col-md-6 col-sm-8">

                                <header className="section-title">
                                <h4 className="pull-left">Sign Up</h4>
                                <p className="pull-right">Have an account <Link to="https://wayfellow.com/login">Sign in</Link></p>
                                <Fragment className="clearfix"></Fragment>
                                </header>
                                <Fragment>
                                  <Input
                                  inputType={"email"}
                                  title={"Email"}
                                  name={"Full name"}
                                  value={this.state.newUser.name}
                                  placeholder={"johndoe@abc.com"}
                                  handleChange={this.handleInput}
                                />{" "}
                                {/* Name of the user */}

                                <Input
                                  inputType={"password"}
                                  title={"password"}
                                  name={"password"}
                                  value={this.state.newUser.name}
                                  placeholder={"password"}
                                  handleChange={this.handleInput}
                                />{" "}
                                {/* Name of the user */}
                                
                                <Input
                                  inputType={"password"}
                                  title={"Comfirm Password"}
                                  name={"password"}
                                  value={this.state.newUser.name}
                                  placeholder={"password"}
                                  handleChange={this.handleInput}
                                />{" "}
                                {/* Name of the user */}


                                <CheckBox
                                title={" "}
                                name={"skils"}
                                options={this.state.skillOptions}
                                selectedOptions={this.state.newUser.skills}
                                handleChange={this.handleCheckBox}
                                />{" "}

                                <Button
                                  action={this.handleFormSubmit}
                                  type={"primary"}
                                  title={"Sighnin"}
                                  style={buttonStyle}
                                />{" "}
                                {/*sighnin */}
                              

                                </Fragment>
                                

                       </Fragment>
                       </Fragment>
                       </Fragment>
         </Fragment>    
      </form>
    );
  }
}

const buttonStyle = {
  margin: "10px 10px 10px 10px"
};

