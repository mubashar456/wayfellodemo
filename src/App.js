import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Home } from './Home';

import { Jobs } from './Jobs';
import {Events} from './Events'
import { Layout } from './components/Layout';
import { NavigationBar } from './components/NavigationBar';
import {Sighnin} from './Sighnin';
import {sighnup} from './sighnup';
import {Footer} from './components/footer';
import {underdev} from './underdev';


class App extends Component {
  state = {
     jobs: [
        {
          id: '1',
          title:'job 1',
          img :''  
        },

        {
          id: '2',
          title:'job 2',
          img :  ''
        },

        {
          id: '2',
          title:'job 2',
          img : '' 
        }
     ]
  }



  render() {
    return (
      <React.Fragment>
        <Router>
          <NavigationBar />
           <Layout>
            <Switch>
              <Route exact path="/home" component={Home} />
              <Route path="/jobs" component={Jobs} />
              <Route path="/events" component={Events} />
              <Route path="/sighnin" component={Sighnin} />
              <Route path="/sighnup" component={sighnup} />
              <Route path ="/underdev" component={underdev} /> 
            </Switch>
          </Layout>
                  <Footer />
        </Router>
      </React.Fragment>
    );
  }
}

export default App;
