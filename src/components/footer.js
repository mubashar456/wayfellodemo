import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';


export const Footer = () =>(
            <div className="page-footer font-small pt-0">
			<div className="mt-2 text-center home-footer">
      
      <Fragment className="footer-copyright mb-1">
      
      <Link to="/home"> wayfellow | </Link>    
       </Fragment>
      <Fragment className="footer-links">
           
                <Link to="/underdev" className="text-muted">About | </Link>
                <Link to="/underdev" className="text-muted">Terms | </Link>
                <Link to="/underdev" className="text-muted" > Contact | </Link>
          
        </Fragment>
   
      <Fragment className="col-sm-12">
            <div className="text-center">
                 <Link to="/underdev" className="text-muted" target="_blank">Facebook </Link>
                 <Link to="/underdev" className="text-muted" target="blank">Linkedinow</Link>
                 <Link to="/underdev" className="text-muted" target="_blank">Twitter</Link> 
            </div>
      </Fragment>    
    
</div>	
	</div> )

