import React from 'react';
import { Link } from 'react-router-dom';
import { Nav, Navbar } from 'react-bootstrap';
import styled from 'styled-components';

const Styles = styled.div`
  .navbar {
    background-color: #4b0082;
  }

  a, .navbar-brand, .navbar-nav .nav-link {
    color: #bbb;

    &:hover {
      color: white;
    }
  }
`;

export const NavigationBar = () => (
  <Styles>
    <Navbar expand="lg">
      <Navbar.Brand>
         wayfellow
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
       <div  className="col-md-10">
      <Nav className="ml-auto">
      
          <Nav.Item>
            <Nav.Link>
              <Link to="/home">home</Link>
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link>
              <Link to="/jobs">jobs</Link>
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
          <Nav.Link>
              <Link to="/events">events</Link>
            </Nav.Link>
          </Nav.Item>
          </Nav>
          </div>
     
          <div className="col-md-2">
           <Nav className="ml">
          <Nav.Item>
            <Nav.Link>
              <Link to="/sighnin">Login</Link>
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link>
              <Link to="/">Register</Link>
            </Nav.Link>
          </Nav.Item>
          
        </Nav>
          </div>
          
      </Navbar.Collapse>
    </Navbar>
  </Styles >
)
